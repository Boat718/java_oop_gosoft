public class App {
    public static void main(String[] args) {
        
        MovableCircle m = new MovableCircle(10, 10, 20, 30, 100);
        m.moveDown();
        m.moveUp();
        m.moveLeft();
        m.moveRight();
    }
}
