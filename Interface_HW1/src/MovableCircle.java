public class MovableCircle extends MovablePoint{

    private int radius;
    private MovablePoint center;

    MovableCircle(int x, int y, int xSpeed, int ySpeed, int radius){
        super(x,y,xSpeed,ySpeed);
        this.radius = radius;
    }

}