public class MovablePoint implements Movable {

    public int x;
    public int y;
    public int xSpeed;
    public int ySpeed;

    MovablePoint(int x, int y, int xSpeed, int ySpeed){
        this.x = x;
        this.y = y;
        this.xSpeed = xSpeed;
        this.ySpeed = ySpeed;
    }

    public String toString(){

        return getClass().getName()+"[x= "+this.x+" y= "+this.y+"]" ;
    }


    @Override
    public void moveUp(){
        System.out.println("MoveUp");
    }

    @Override
    public void moveDown(){
        System.out.println("MoveDown");
    }

    @Override
    public void moveLeft(){
        System.out.println("MoveLeft");
    }

    @Override
    public void moveRight(){
        System.out.println("MoveRight");
    }
    
}
