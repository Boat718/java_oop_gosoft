

public class App {
    public static void main(String[] args){

        Employee employee = new Employee("Steve", "Roger", 9999);
        CEO ceo = new CEO("Tony", "Stark",999999);
        
        ceo.gossip();
        employee.gossip();
        ceo.fire(employee);
        ceo.hire(employee);
        ceo.seminar();
        ceo.golf();
        ceo.assignNewSalary(employee, 200);
        ceo.assignNewSalary(employee, 19999);
        
    }
}