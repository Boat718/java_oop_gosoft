public class CEO extends Employee {

    public Programmer[] employees;

    public CEO(String firstnameInput, String lastnameInput, int salaryInput) {
        super(firstnameInput, lastnameInput, salaryInput);
        
    }

    public int getSalary(){
        return super.getSalary()*2;
    };

    public void work (Employee luckyEmployee) {
        this.fire(luckyEmployee);
        this.hire(luckyEmployee);
        this.seminar();
        this.golf();
    }

    public void assignNewSalary(Employee luckyEmployee, int newSalary) {

        if(newSalary < luckyEmployee.getSalary()){
            System.out.println(luckyEmployee.firstname+"'s "+"salary is less than before!!");
        }else{
            System.out.println(luckyEmployee.firstname+"'s "+"salary has been set to "+ newSalary);
        }

    }

    public void golf () { 
        this.dressCode = "golf_dress";
        System.out.println("He goes to golf club to find a new connection. Dress with :" + this.dressCode);
    };

    public void fire(Employee luckyEmployee) {
        this.dressCode = "tshirt";
        System.out.println(luckyEmployee.firstname+" has been fired! Dress with: "+this.dressCode);
    }

    public void hire(Employee luckyEmployee){
        this.dressCode = "tshirt";
        System.out.println(luckyEmployee.firstname+" has been hired back Dress with: "+this.dressCode);
    }

    public void seminar(){
        this.dressCode = "suit";
        System.out.println("He is going to seminar Dress with: "+this.dressCode);
    }
}