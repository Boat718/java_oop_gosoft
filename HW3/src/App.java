
public class App {
    public static void main(String[] args) {
        String[] rawData = {
            "id:1001 firstname:Luke lastname:Skywalker salary:10000 type:frontend",
            "id:1002 firstname:Tony lastname:Stark salary:20000 type:backend",
            "id:1003 firstname:Somchai lastname:Jaidee salary:30000 type:fullstack",
            "id:1004 firstname:MonkeyD lastname:Luffee salary:40000 type:fullstack"
        };

        CEO ceo = new CEO("firstnameInput", "lastnameInput", 20);
        addProgrammer(ceo,rawData);

        for (Programmer p : ceo.employees) {
            System.out.println("ID: "+p.id+" Firstname: "+p.firstname+" Lastname: "+p.lastname+ " Salary: "+p.getSalary()+" Type: "+p.type);
        }
    }

    public static void addProgrammer(CEO ceo, String[] rawData){

       Programmer[] programmers = new Programmer[rawData.length];

       for(int i = 0; i < rawData.length; i++){
   
        String[] str = rawData[i].split(" ");
        String[] arr = new String[str.length];
        
            for(int j = 0; j < str.length ; j++){

                    String[] str2 = str[j].split(":");
                    
                    arr[j] = str2[1];                   
                }
                
        programmers[i] = new Programmer(Integer.valueOf(arr[0]), arr[1], arr[2],Integer.valueOf(arr[3]) , arr[4]);
        
       }

       ceo.employees = programmers;
    }
}
