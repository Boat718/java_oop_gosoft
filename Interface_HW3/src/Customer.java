public class Customer {

    private  String name;
    private boolean member = false;
    private String memberType;

    Customer(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public boolean isMember(){
        return false;
    }

    public void setMember(boolean member){
        this.member = member;
    }

    public String getMemberType(){
        return memberType;
    }

    public void setMemberType(String type){
        this.memberType = type;
    }

    public String toString(){
        return getClass().getName()+"[ name: "+this.name+ " member: "+this.member+ " memberType: "+this.memberType+"]";
    }

}
