import java.util.Date;

public class DiscountRate extends Visit {
    
    private double serviceDiscountPremium = 0.2;
    private double serviceDiscountGold = 0.15;
    private double serviceDiscountSilver = 0.1;
    private double productDiscountPremium = 0.1;
    private double productDiscountGold = 0.1;
    private double productDiscountSilver= 0.1;
    
    DiscountRate(String name, Date date) {
        super(name, date);
        //TODO Auto-generated constructor stub
    }
    

    public double getServiceDiscountRate (String type){
        if(type == "Premium"){
            return this.getServiceExpense() * this.serviceDiscountPremium;
        }
        else if (type == "Gold"){
            return this.getServiceExpense() * this.serviceDiscountGold;
        }
        
        return this.getServiceExpense() * this.serviceDiscountSilver;
    }

    public double getProductDiscountRate(String type){

        if(type == "Premium"){
            return this.getProductExpense() * this.productDiscountPremium;
        }
        else if (type == "Gold"){
            return this.getProductExpense() * this.productDiscountGold;
        }
        
        return this.getProductExpense() * this.productDiscountSilver;
    }
}
