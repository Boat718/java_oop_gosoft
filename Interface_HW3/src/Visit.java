import java.util.Date;

public class Visit extends Customer {
    private Customer customer;
    private  Date date;
    private double serviceExpense;
    private double productExpense;

    Visit(String name, Date date){
        super(name);
        this.date = date;
    }

    @Override
    public String toString(){
        return getClass().getName()+" "+this.getName()+" "+this.date.toString();
    }

    public double getServiceExpense(){
        return this.serviceExpense;
    }

    public void setServiceExpense(double serviceExpense){
        this.serviceExpense = serviceExpense;
    }

    public double getProductExpense(){
        return this.productExpense;
    }

    public void setProducExpense(double productExpense){
        this.productExpense = productExpense;
    }

    public double getTotalExpense(){
        return this.productExpense + this.serviceExpense;
    }

}
