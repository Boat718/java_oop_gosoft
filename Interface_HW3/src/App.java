import java.util.Date;

public class App {
    public static void main(String[] args){

       DiscountRate x = new DiscountRate("Google", new Date());
       x.setMemberType("Gold");
       x.setServiceExpense(56789);
       System.out.println(x.getServiceDiscountRate(x.getMemberType()));

    }
}
