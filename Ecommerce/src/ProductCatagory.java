import java.util.ArrayList;

public class ProductCatagory {
    public ArrayList<Product> products = new ArrayList<>();

    public void showProduct(){
        System.out.println("Product in Category");
        for(int i = 0; i < this.products.size();i++){
            System.out.printf("ID: %s Name: %s Price: %d \n",this.products.get(i).id,this.products.get(i).name,this.products.get(i).price);
        }    
    }
    public void addProduct(Product input){
        this.products.add(input);
    }
    public void removeProduct(String id){
        this.products.removeIf(e -> e.id.equals(id));
    }
    public Product getProductById(String id){
        for(Product product: this.products){
            if(product.id.equals(id)){
                return product;
            }
        }
        Product p = new Product(-1, "name", "-1");
        return p;
    }

}
