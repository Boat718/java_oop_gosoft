import java.util.ArrayList;

public class State {
    private static State instance;

    public static State getInstance(){
        if(instance == null){
            instance = new State();
        }
        return instance;
    }

    public Mode isAdmin = Mode.Customer;
    public boolean isHome = true;
    public ArrayList<Product> cart = new ArrayList<>();
    public void showProducts(){
        System.out.println("Products in cart");
        for(int i = 0; i < cart.size();i++){
            System.out.printf("ID : %s Name : %s  Price :%d\n" , this.cart.get(i).id , this.cart.get(i).name ,this.cart.get(i).price);
        }
    }
    public void addProductById(String id, ProductCatagory pc){
        Product p = pc.getProductById(id);
        this.cart.add(p);
    }
    public void removeProduct(String id){
        this.cart.removeIf(e->e.id.equals(id));
    }
}
