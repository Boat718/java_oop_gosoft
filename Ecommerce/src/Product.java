public class Product extends ProductAbstract implements Iproduct {
    
    Product(int price, String name, String id){
        this.price = price;
        this.name = name;
        this.id = id;
    }

    @Override
    public String toString(){
        return getClass().getName()+ "\t [ ID = "+id+ ", Name = "+name+ ", Price ="+ price+ "]";
    }

    @Override
    public void showData(){
        System.out.println(super.name);
    }
}
