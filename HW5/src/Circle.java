public class Circle {
    private  double radius = 1.0;
    private String color = "red";

    Circle(){
    }

    Circle(Double r){
        this.radius = r;
    }

    public double getRadius(){
        return this.radius;
    }

    public double getArea(){
        return this.radius*this.radius*3.14;
    }
}
